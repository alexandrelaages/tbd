package controller;

import java.util.List;

import javax.inject.Inject;

import modelo.Aluno;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import dao.AlunoDao;

@Controller
public class AlunoController {

	private AlunoDao alunos;
	private Result result;
	
	protected AlunoController(){
		this(null, null);
	}
	
	@Inject
	public AlunoController(AlunoDao alunos, Result result){
		this.alunos = alunos; 
		this.result = result;
	}
	
	@Get("/aluno/lista")
	public void listar(){
		List<Aluno> listaDeAlunos = alunos.listar();
		result.use(Results.xml()).from(listaDeAlunos).serialize();
	}
}