package controller;

import java.util.List;

import javax.inject.Inject;

import modelo.Curso;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import dao.CursoDao;

@Controller
public class CursoController {

	private CursoDao cursos;
	private Result result;
	
	protected CursoController(){
		this(null, null);
	}
	
	@Inject
	public CursoController(CursoDao cursos, Result result){
		this.cursos = cursos;
		this.result = result;
	}
	
	@Get("/curso/lista")
	public void listar(){
		List<Curso> listaDeCursos = cursos.listar();
		result.use(Results.xml()).from(listaDeCursos).serialize();
	}
}