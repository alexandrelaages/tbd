package dao;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import modelo.Curso;

import org.hibernate.Session;

@RequestScoped
public class CursoDao {

	private final Session session;
	
	public CursoDao(){
		this(null);
	}
	
	@Inject
	public CursoDao(Session session){
		this.session = session;
	}
	
	@SuppressWarnings("unchecked")
	public List<Curso> listar(){
		return this.session.createCriteria(Curso.class)
				.list();
	}
}