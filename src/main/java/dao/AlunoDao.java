package dao;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import modelo.Aluno;

import org.hibernate.Session;

@RequestScoped
public class AlunoDao {

	
	private Session session;
	
	public AlunoDao(){
		this(null);
	}
	
	@Inject
	public AlunoDao(Session session){
		this.session = session;
	}
	
	@SuppressWarnings("unchecked")
	public List<Aluno> listar(){
		return this.session.createCriteria(Aluno.class)
				.list();
	}
}